from download_file import download_file
from upload_file import upload_file

def main():
    print("upload_file...")
    upload_file()
    print("download_file...")
    download_file()


if __name__ == '__main__':
    main()
