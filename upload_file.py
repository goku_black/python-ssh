import paramiko
from scp import SCPClient

from settings import HOST
from settings import PORT
from settings import USERNAME
from settings import PASSWORD

from settings import UPLOAD_SERVER_REMOTE_PATH
from settings import UPLOAD_LOCAL_FILE_PATG


def upload_file():
    """
    上传文件
    :return:
    """
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh_client.connect(HOST, PORT, USERNAME, PASSWORD)
    scp_client = SCPClient(ssh_client.get_transport(), socket_timeout=15.0)
    try:
        scp_client.put(UPLOAD_LOCAL_FILE_PATG, UPLOAD_SERVER_REMOTE_PATH)
    except FileNotFoundError as e:
        print(e)
        print("系统找不到指定文件" + UPLOAD_LOCAL_FILE_PATG)
    else:
        print("文件上传成功")
    ssh_client.close()
