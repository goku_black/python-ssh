#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@author:huhao
@file:settings.py.py
@time:2021/07/06
"""
# 公共配置
HOST = "xx.xx.xx.xx"  # 服务器ip地址
PORT = 22  # 端口号
USERNAME = "xxxxxx"  # 用户名
PASSWORD = "xxxxxxx"  # 密码

# 上传文件的配置
UPLOAD_SERVER_REMOTE_PATH = "/opt"  # 要上传的目标路径
UPLOAD_LOCAL_FILE_PATG = "/Users/xxx/2.txt"  # 本地文件的路径

# 下载文件的配置
DOWNLOAD_SERVER_REMOTE_PATH = "/opt/3.txt"  # 要下载的服务器上文件的位置
DOWNLOAD_LOCAL_FILE_PATH = "/Users/xxx"  # 本地路径
