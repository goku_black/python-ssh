import paramiko
from scp import SCPClient

from settings import HOST
from settings import PORT
from settings import USERNAME
from settings import PASSWORD

from settings import DOWNLOAD_LOCAL_FILE_PATH
from settings import DOWNLOAD_SERVER_REMOTE_PATH


def download_file():
    """
    下载文件
    :return:
    """
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh_client.connect(HOST, PORT, USERNAME, PASSWORD)
    scp_client = SCPClient(ssh_client.get_transport(), socket_timeout=15.0)
    try:
        scp_client.get(DOWNLOAD_SERVER_REMOTE_PATH, DOWNLOAD_LOCAL_FILE_PATH)
    except FileNotFoundError as e:
        print(e)
        print("系统找不到指定文件" + DOWNLOAD_SERVER_REMOTE_PATH)
    else:
        print("文件下载成功")
    ssh_client.close()
